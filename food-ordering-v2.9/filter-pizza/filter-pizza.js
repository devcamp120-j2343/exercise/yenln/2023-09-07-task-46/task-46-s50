$(document).ready(function () {
    $("#slider-range").slider({
        range: true,
        values: [15, 30],
        min: 15,
        max: 30,
        step: 1,
        slide: function (event, ui) {
            $("#min-price").html(ui.values[0]);

            $("#max-price").html(ui.values[1]);

            $("#slider-price-min").val(ui.values[0])
            $("#slider-price-max").val(ui.values[1]);

            //create query parameters
            let vQueryParams = new URLSearchParams({});
            //get query params
            vQueryParams = getQueryParameters(vQueryParams);
            loadProductsByPagination(1, vQueryParams);

        }
    });

})

let gPerpage = 9; //number of products per page
let gTotalPages = 0; //total page number of products


$(document).ready(function () {
    onPageLoading();

    $(".add-a-product").on("click",function(){
        checkCart(this);
    })

    //handle rating filter change
    $("input[type='checkbox']").on("change", () => {
        let vQueryParams = new URLSearchParams();

        vQueryParams = getQueryParameters(vQueryParams);
        loadProductsByPagination(1, vQueryParams);
    })
})

function onPageLoading() {
    'use strict'
    let vPage = 1; //Loading with first page

    loadProductsByPagination(vPage);
}

function loadProductsByPagination(paramPageNumber, paramQuery) {
    let vQueryParams = null;
    if (!paramQuery) {
        vQueryParams = new URLSearchParams({
            _limit: gPerpage,
            _page: paramPageNumber - 1,
        });
    } else {
        vQueryParams = paramQuery;
    }
    $.ajax({
        url:
            "https://food-ordering-fvo9.onrender.com/api/foods?" + vQueryParams.toString(),
        type: "get",
        dataType: "json",
        success: function (res) {
            console.log(res);
            gTotalPages = Math.ceil(res.count / gPerpage);
            //Tạo trang
            handlePagination(paramPageNumber);
            //xoá trắng phần tử cũ
            $(".list-pizza-kitchen-section").html('');
            res.rows.forEach(product => {
                $(".list-pizza-kitchen-section").append(`
                <div class="product-detail-kitchen">
                <div class="img-of-pizza-detail">
                    <img alt="" src="${product.imageUrl}">
                </div>
                <div class="information-each-pizza-detail">
                    <div>
                        <span class="title-pizza-kitchen">${product.name}</span>
                        <span class="title-pizza-kitchen">$${product.price}</span>
                    </div>
                    <div>
                        <div>
                            <span class="has-border-detail"><i class="fa-solid fa-star"></i>&nbsp;${product.rating}</span>
                            <span class="has-border-detail">${product.time}</span>
                        </div>
                        <button class="add-a-product" id="add-cart-${product.id}">
                            <span class="choosing-add-outside"><i class="fa-solid fa-plus"></i></span>
                        </button>
                    </div>
                </div>
            </div>
`)
                $(`#add-cart-${product.id}`).on("click",()=>{
                    addToCart(product);
                })
            });

        },
        error: function () {
            $(".list-pizza-kitchen-section").html("error");
        },
    })
}

function addToCart(paramProduct){
    paramProduct.quantity = 1;
    var vCartItems = localStorage.getItem("shopping") ? JSON.parse(localStorage.getItem("shopping")) : [];
    if(vCartItems.length > 0){
        var vExist = vCartItems.filter((item)=>{
            return item.id == paramProduct.id;
        })
        if(vExist.length > 0){
            vExist[0].quantity++;
        }else{
            vCartItems.push(paramProduct);
        } 
    }
    else{
        vCartItems.push(paramProduct);
    }
    localStorage.setItem("shopping",JSON.stringify(vCartItems));
    checkCart();
}

function handlePagination(paramPageNumber) {
    $("#paging-container").html("");

    for (let bI = 0; bI <= 3; bI++) {
        if (paramPageNumber == paramPageNumber + bI) {
            $("#paging-container").append(`
            <li class="page-item active "><a href="javascript:void(0)" class="page-link bg-pagination-light" href="#">${paramPageNumber + bI}</a></li>
            `);
        } else if (paramPageNumber + bI <= gTotalPages) {
            $("#paging-container").append(` <li class="page-item" onclick="loadProductsByPagination(${paramPageNumber + bI})">
              <a href="javascript:void(0)" class="page-link">
                  ${paramPageNumber + bI}
              </a></li>
            `)
        }
    }
}

function getQueryParameters(paramQuery){
    //slider
    let vPriceMin = $("#slider-price-min").val();
    let vPriceMax = $("#slider-price-max").val();

    //rating
    let vRatingFiveStar = $("#5-stars-check-box").is(":checked");
    let vRatingFourStar = $("#4-stars-check-box").is(":checked");
    let vRatingThreeStar = $("#3-stars-check-box").is(":checked");
    let vRatingTwoStar = $("#2-stars-check-box").is(":checked");
    let vRatingOneStar = $("#1-stars-check-box").is(":checked");

    //add price ranger query
    paramQuery.append("priceMin", parseInt(vPriceMin));
    paramQuery.append("priceMax", parseInt(vPriceMax));
    //add rating query

    if(vRatingOneStar){
        paramQuery.append("rating",1);
    }
    if(vRatingTwoStar){
        paramQuery.append("rating",2);
    }
    if(vRatingThreeStar){
        paramQuery.append("rating",3);
    }
    if(vRatingFourStar){
        paramQuery.append("rating",4);
    }
    if(vRatingFiveStar){
        paramQuery.append("rating",5);
    }
    return paramQuery;
}

function checkCart(){
    var vCartItems = localStorage.getItem("shopping") ? JSON.parse(localStorage.getItem("shopping")) : [];
    if(vCartItems.length > 0 ){
        $("#shopping-bag-icon").attr("src", "images/yellow-shopping-bag.png");
    }
}

